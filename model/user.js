const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const jsSHA = require('jssha');
const emailValidator = require('../utils/email-validator');

const UserSchema = new Schema({
  firstName: {type: String, required: true},
  lastName: {type: String, required: true},
  email: {
    type: String,
    required: true,
    unique: true,
    match: emailValidator.regex
  },
  userName: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  active: {type: Boolean, required: true, 'default': true},
  createdAt: {type: Date, 'default': new Date()},
  timezone: String,
  roles: {type: Array, 'default': ['USER']}
});

UserSchema.methods.getHash = function (sText) {
  //I know that is unsafe, but it's a student project anyway
  const oSha512 = new jsSHA('SHA-512', 'TEXT');
  oSha512.update(sText);
  return oSha512.getHash('B64');
};

UserSchema.methods.validatePassword = function (sPassword) {
  const sHash = this.getHash(sPassword);
  return sHash === this.password;
};

UserSchema.methods.hashPassword = function (sPassword) {
  if (sPassword) {
    this.password = this.getHash(sPassword);
  } else {
    this.password = this.getHash(this.password);
  }
};

mongoose.model('User', UserSchema);