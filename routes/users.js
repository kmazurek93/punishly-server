const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = mongoose.model('User');
const authChecker = require('../utils/auth-checker');

router.all('/edit/*', authChecker.authenticateRequest);
router.all('/list/*', authChecker.authenticateRequest);
router.all('/profile/*', authChecker.authenticateRequest);
router.all('/context/*', authChecker.authenticateRequest);

router.post('/register', async function (req, res, next) {
  let user = new User(req.body);
  user.hashPassword(user.password);
  user.roles = ['USER'];
  try {
    user = await user.save();
    return res.json(user);
  } catch (err) {
    next(err);
  }
});

router.get('/list', async function (req, res, next) {
  try {
    const users = await User.find({})
    .select('-password')
    .exec();
    res.json(users);
  } catch (err) {
    next(err);
  }
});

router.put('/edit/:id', async function (req, res, next) {
  const _paramId = req.param.id;
  const userToSave = new User(req.body);
  if (_paramId !== userToSave._id) {
    return res.sendStatus(400).end();
  }
  const options = {new: true};
  const conditions = {_id: userToSave._id};
  const currentUser = req.user;
  let isAdmin = hasRole(currentUser, 'ADMIN');

  if (canEditThisUser(currentUser, userToSave) || editsOwnProfile(currentUser,
          userToSave)) {
    if (!isAdmin) {
      userToSave.roles = currentUser.roles;
    }
    User.findOneAndUpdate(conditions, userToSave, options,
        function (err, savedUser) {
          if (err) {
            next(err);
          } else {
            return res.json(savedUser);
          }
        });
  } else {
    res.status(403);
    res.json({msg: ['FORBIDDEN']});
    return res.end();
  }
});

router.get('/profile/:id', async function (req, res, next) {
  const id = req.params['id'];
  if (id === null || id === undefined) {
    res.sendStatus(400);
  } else {
    User.findById(id, function (err, user) {
      if (err) {
        next(err);
      } else {
        res.json(transformToProfile(user));
      }
    });
  }
});

router.get('/context', async function (req, res, next) {
  const oCurrentUser = req.user;
  const oUserContext = {
    userName: oCurrentUser.userName,
    roles: oCurrentUser.roles,
    timezone: oCurrentUser.timezone
  };
  res.json(oUserContext);
});

function hasRole(user, role) {
  return user.roles.contains(role);
}

function canEditThisUser(currentUser, userToSave) {
  return (hasRole(currentUser, 'ADMIN') && currentUser._id !== userToSave._id);
}

function editsOwnProfile(currentUser, userToSave) {
  return currentUser._id === userToSave._id;
}

function transformToProfile(user) {
  return {
    firstName: user.firstName,
    lastName: user.lastName,
    userName: user.userName,
    email: user.email,
    timezone: user.timezone,
    roles: user.roles,
    active: user.active
  };
}

module.exports = router;
