const passport = require('passport');

module.exports = function (app) {
  app.post('/login',
      passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
      })
  );

  function handleLoginRedirect(req, res, next) {
    let msg = req.flash('loginErr');
    if (msg.length === 0) {
      msg = ['PLEASE_LOGIN'];
    } else {
      res.status(401);
    }
    res.redirect('/');
  }

  function doLogout(req, res, next) {
    req.logout();
    res.json({msg: 'logout success'});
  }

  function getLoginStatus(req, res, next) {
    if (req.user) {
      res.json(req.user.roles);
    } else {
      res.json([]);
    }
  }

  app.route('/login')
  .get(handleLoginRedirect)
  .put(handleLoginRedirect)
  .delete(handleLoginRedirect);

  app.route('/loginStatus')
  .get(getLoginStatus);

  app.route('/logout')
  .get(doLogout)
  .put(doLogout)
  .post(doLogout)
  .delete(doLogout);
};