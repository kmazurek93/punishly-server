module.exports = {
  apps: [{
    name: "punishly-server",
    script: "./bin/www",
    watch: true,
    env: {
      "NODE_ENV": "production",
      "ODBLUZG": "odbluzg",
      "PORT": 80,
      "PORT_SSL": 443
    },
  }]
};