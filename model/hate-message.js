const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HateMessage = new Schema({
  userId: {ref: 'User', type: Schema.Types.ObjectId},
  text: {type: String, required: true},
  createdAt: {type: Date, 'default': new Date()},
  visible: {type: Boolean, 'default': true},
  tags: {type: Array}
});

mongoose.model('HateMessage', HateMessage);