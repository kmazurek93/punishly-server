exports.authenticateRequest = function(req, res, next) {
  if(req.isAuthenticated()) {
    next();
  } else {
    redirectToLogin(req, res);
  }
};

function redirectToLogin(req, res) {
  res.json({msg: 'UNAUTHORIZED'});
}

exports.hasAdminPrivileges = function(req, res, next) {
  const roles = req.user.roles;
  if(roles.indexOf('ADMIN') !== -1) {
    next();
  }  else {
    const output = {msg: ['UNAUTHORIZED']};
    res.status(401);
    res.json(output);
    res.end();
  }
};