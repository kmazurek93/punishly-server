const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const HateMessage = mongoose.model('HateMessage');
const authChecker = require('../utils/auth-checker');
const censor = require('../ext/censor')();


router.all('*', authChecker.authenticateRequest);

router.get('/byUser/:id', async function (req, res, next) {
  try {
    const hatesByUser = await HateMessage
    .find({userId: req.param.id})
    .sort({createdAt: -1})
    .exec();

    return res.json(hatesByUser);
  } catch (err) {
    next(err);
  }
});

router.post('/new', async function (req, res, next) {
  const _id = req.user._id;
  let msg = new HateMessage(req.body);
  msg.text = await censor.doCensoring(msg.text);
  msg.userId = _id;
  try {
    msg = await msg.save();
    return res.json(msg);
  } catch (err) {
    next(err);
  }
});

module.exports = router;