const child_process = require('child_process');
const REM_VULG_CMD = process.env.ODBLUZG;

const FALLBACK = 'cat';

module.exports = function () {
  function trimAll(buffer) {
    return buffer.toString().replace(/(^Input.string: )/g, "").replace(
        /^Output.string:./g, "");

  }

  function doCensoring(text) {
    return new Promise((resolve, reject) => {
          let buffer = Buffer.alloc(text.length + 100);
          let pos = 0;
          const proc = child_process.spawn(REM_VULG_CMD ? REM_VULG_CMD : FALLBACK);
          proc.stdout.on('data', (data) => {
            pos += buffer.write(trimAll(data), pos);
          });
          proc.on('error', (err) => reject(err));
          proc.stdout.on('close', () => {
            return resolve(buffer.toString().replace(/\0/g, '').trim());
          });
          proc.stdin.write(text);
          proc.stdin.end();
        }
    );

  }

  return {doCensoring};
};