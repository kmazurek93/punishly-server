const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const User = mongoose.model('User');

console.log('local strategy setup');
passport.use(new LocalStrategy(
    {
      passReqToCallback: true
    },
    function (req, username, password, done) {
      const conditions = {userName: username};
      User.findOne(conditions, function (err, user) {
        if (err) {
          console.log(err);
          return done(err);
        } else if (!user) {
          console.log('UserNotFound: ' + username);
          return done(null, false, req.flash('loginErr', 'BAD_CREDENTIALS'));
        } else if (user.validatePassword(password) && user.active) {
          return done(null, user);
        } else {
          console.log('Bad Credentials for user: ' + username);
          return done(null, false, req.flash('loginErr', 'BAD_CREDENTIALS'));
        }
      });
    }
));

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});