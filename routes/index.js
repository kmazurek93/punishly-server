var express = require('express');
var router = express.Router();

/* GET home page. */
//TODO serve static.
router.get('/', function(req, res, next) {
  res.locals.user = req.user;
  res.render('index', { title: 'Express' });
});

module.exports = router;
