const mongoose = require('mongoose');
const Promise = require('bluebird');

module.exports = function () {
  const db = mongoose.connect('mongodb://localhost/punishly');
  mongoose.Promise = Promise;

  //register models
  require('../model/user');
  require('../model/hate-message');

  return db;
};